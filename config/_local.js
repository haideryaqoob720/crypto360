config = {
    defaults: {
        cryptoCurrency: 'BTC',
        fiatCurrency: 'EUR',
    },
    network: 'testnet',
    server: {
        logs: {
            level: 'info',
            logFolder: './logs/'
        },
        name: 'vault_manager',
        exchange: 'GCU',
        port: 8000,
        connection: 'http',
        post_limit: 1024,
        requestTimoutCommunicator: 60 * 1000, //60 sec timout if microservices doesn't respond
        optimalThresholdUpdateInterval: 4 * 60 * 60 * 1000 //4h
    },

    externalServers: {
        accounting: 'http://localhost:3060',
        tms: '5.189.149.113:3050',
        order_api: '5.189.149.113:3040',
        kycServer: 'http://localhost:7273',
        fiat_server: 'http://localhost:8002/',
        rateEngine: 'http://5.189.149.113:8082'
    },

    apis: {
        sms: {
            url: 'https://api.smsapi.com/',
            token: '72bMMseiAugU9IUcKfdlvTBZIZkCaDT0YT0Ch8dx',
            from: 'GSUEXCHANGE',
        }
    },

    chains: {
        eth: {
            tx: 'eth.wallet.transaction',
            tx_confirmed: 'eth.wallet.transaction.confirmed',
            withdrawal: 'eth.wallet.withdrawal.confirmed',
            command: 'eth.wallet.command',
            command_reply: 'eth.wallet.command.repl'
        },
        btc: {
            tx: 'btc.wallet.transaction',
            tx_confirmed: 'btc.wallet.transaction.confirmed',
            command: 'btc.wallet.command',
            command_reply: 'btc.wallet.command.repl'
        },
        toft: {
            tx: 'toft.wallet.transaction',
            tx_confirmed: 'toft.wallet.transaction.confirmed',
            command: 'toft.wallet.command',
            command_reply: 'toft.wallet.command.repl'
        }
    },
    kafka: {
        hosts: ['localhost:9092'],
        name: 'vaultManager',
        consumer: {
            topics:
                [
                    'btc.wallet.transaction.confirmed',
                    'btc.wallet.transaction',
                    'btc.wallet.command.reply',
                    'errors',
                    'withdrawal'
                ]
        },
        producers: {
            commands: {},
            pollInterval: 100,
            topics:
                [
                    'btc.wallet.command'
                ],
            _topics: {
                fiat: 'fiat.wallet.command',
                accounting: 'accounting',
                notifyOMS: 'command'
            }
        },
        event_hub: {
            producers: {
                topics:
                    [
                        'vault.manager',
                        'deposit',
                        'withdrawal',
                        'command'
                    ],
                _topics: {
                    admin: 'admin',
                    sendEmail: 'emails',
                    vma_revenue: 'revenue'
                }
            },
        }
    },
    database: {
        connection: {
            host: 'localhost',
            port: 3300,
            // user: 'eth-wallet',
            user: 'root',
            password: 'my-secret-pw',
            database: 'vault_db',
        },
        pool: {
            min: 2,
            max: 10,
        }
    }
}
module.exports = config;
