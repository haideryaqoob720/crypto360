const express = require('express');
// var Kafka = require('node-rdkafka');
const bodyParser = require("body-parser");
const cors = require("cors");
const helper = require("./helpers/helper");

const app = express();


var corsOptions = {
  origin: "http://localhost:8080"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
const events = require("../src/helpers/kafka")
const db = require("../src/models");
const Role = db.role;


db.sequelize.sync({ force: false }).then(() => {
  console.log("Drop and re-sync db.");
  initial();
});

helper.init().then(()=> {
  console.log("request for connection to kafka sent");
}).catch((error)=> {
  console.log("unable to connect kafka: ", error);
})
// events.connectProducer().then(() => {
//   console.log("producer connected in app file");
// });
// events.connectConsumer().then(() => {
//   console.log("consumer connected in app file");
// });
// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Crypt Payment Gateway." });
});

const { authJwt } = require("../src/middleware");
// routes
require("../src/routes/tutorial.routes")(app);
require('../src/routes/auth.routes')(app);
require('../src/routes/user.routes')(app);
require('../src/routes/commentOnTutorial.routes')(app);
require('../src/routes/merchantApiKey.routes')(app);
require('../src/routes/address.routes')(app);
require('../src/routes/forgetPassword.routes')(app);
require('../src/routes/deposit.route')(app);
require('../src/routes/merchant.routes')(app);
require('../src/routes/changePassword.routes')(app);

app.post('/withdraw',[authJwt.verifyToken] , require('../src/routes/withdraw.route'))

// set port, listen for requests
const PORT = process.env.PORT || 8085;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});


/*===================================
Producer
=====================================*/
// var data = {"command":"create_account","commandTopic":"btc.wallet.command","data":{"user_id":"1","symbol":"BTC","chain":"BTC"}}
// var myJSON = JSON.stringify(data);

// var consumers = ['btc.wallet.command.reply','btc.wallet.transaction.confirmed','btc.hot.wallet.creation.confirmed','btc.revenue.withdrawal.confirmed',
// 'btc.exchange.hot.wallets','btc.expenses_wallet', 'btc.errors', 'btc.coldwallets']

// var producer = new Kafka.Producer({
//     'metadata.broker.list': 'localhost:9092',
//     'dr_cb': true
//   });
  

//   producer.connect();
  
 
//   producer.on('ready', function() {
//       console.log("producer successfully connected")
//     try {
//       producer.produce(
  
//         'btc.wallet.command',
      
//         null,
      
//         Buffer.from(myJSON),
        
//         'Stormwind',

//         Date.now(),
//       );
//     } catch (err) {
//       console.error('A problem occurred when sending our message');
//       console.error(err);
//     }
//   });
  
//   producer.on('event.error', function(err) {
//     console.error('Error from producer');
//     console.error(err);
//   })
  
//   producer.setPollInterval(100);

// /*===================================
// Consumer
// =====================================*/

// var consumer = new Kafka.KafkaConsumer({
//     'group.id': 'kafka',
//     'metadata.broker.list': 'localhost:9092',
//   }, {});

// consumer.connect();
// console.log("consumer successfully connected")

// consumer
//   .on('ready', function() {
//     consumer.subscribe(['errors']);
//     consumer.consume();
//   })
//   .on('data', function(data) {
//     console.log(data.value.toString());
//   });

  function initial() {
    Role.create({
      id: 1,
      name: "user"
    });
   
    Role.create({
      id: 2,
      name: "merchant"
    });
   
    Role.create({
      id: 3,
      name: "admin"
    });
  }

  process.on('SIGINT', async function () {
    console.log('SIGINT Shutdown received.');
    await helper.closeConnections();
    process.exit(0)
});

// //Terminate active connection on kill
process.on('SIGTERM', async function () {
    console.log('SIGTERM Shutdown received.');
    await helper.closeConnections();
    process.exit(0)
});