const db = require("../models");
const Tutorial = db.tutorials;
const Comment = db.comments;

const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.createTutorial = (req, res) => {
    // Validate request
    if (!req.body.title) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    // Create a Tutorial
    const tutorial = {
      title: req.body.title,
      description: req.body.description,
      published: req.body.published ? req.body.published : false
    };
  
    // Save Tutorial in the database
    Tutorial.create(tutorial)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial."
        });
      });
  };

// exports.createTutorial = (tutorial) => {
//     return Tutorial.create({
//       title: tutorial.title,
//       description: tutorial.description,
//       published: tutorial.published
//     })
//       .then((tutorial) => {
//         console.log(">> Created tutorial: " + JSON.stringify(tutorial, null, 4));
//         return tutorial;
//       })
//       .catch((err) => {
//         console.log(">> Error while creating tutorial: ", err);
//       });
//   };
exports.createComment = (req, res) => {
    // Validate request
    if (!req.body.name) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    // Create a Tutorial
    const comment = {
      name: req.body.name,
      text: req.body.text,
      tutorialId: req.body.tutorialId
    };
  
    // Save Tutorial in the database
    Comment.create(comment)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial."
        });
      });
  };

//   exports.createComment = (tutorialId, comment) => {
//     return Comment.create({
//       name: comment.name,
//       text: comment.text,
//       tutorialId: tutorialId,
//     })
//       .then((comment) => {
//         console.log(">> Created comment: " + JSON.stringify(comment, null, 4));
//         return comment;
//       })
//       .catch((err) => {
//         console.log(">> Error while creating comment: ", err);
//       });
//   };

  exports.findTutorialById = (tutorialId) => {
    return Tutorial.findByPk(tutorialId, { include: ["comments"] })
      .then((tutorial) => {
        return tutorial;
      })
      .catch((err) => {
        console.log(">> Error while finding tutorial: ", err);
      });
  };

  exports.findCommentById = (id) => {
    return Comment.findByPk(id, { include: ["tutorial"] })
      .then((comment) => {
        return comment;
      })
      .catch((err) => {
        console.log(">> Error while finding comment: ", err);
      });
  };


  exports.findAll = () => {
    return Tutorial.findAll({
      include: ["comments"],
    }).then((tutorials) => {
      return tutorials;
    });
  };
