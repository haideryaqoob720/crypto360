
kafka = require('../helpers/kafka');
const btcCommandReply = 'btc.wallet.command';

const db = require("../models");
const Address = db.address;
const merchantApikey = db.merchantApikey;

const Op = db.Sequelize.Op;
const _ = require('underscore')
const config = require('../../config/_local')
// const moment = require('moment')()

// const db = require('../db/db')
// const queue = require('../helpers/queue')
// const helper = require('../helpers/helper')

// const log = require('../helpers/logger').create('address')
const consumerTopics = config.kafka.consumer.topics //getting objects of supported chains
const eventhub = config.kafka.event_hub.producers.topics[1] //vault.manager


//filtering only command.reply topics

// which elements in the passed array satisfy the condition

var filteredTopics = _.filter(consumerTopics, function (topic) {
    console.log("filteredTopic: ", topic.toString().indexOf("command.reply") > -1 )
    return topic.toString().indexOf("command.reply") > -1
})

module.exports = {
    new: async (req, res) => {
        if(req.body.clientInvoice == null){
            return res.status(200).send('Please insert client invoice id');
        }
        var clientInvoice = await Address.findOne({ where: { clientInvoice: req.body.clientInvoice}});
        if(clientInvoice != null){
            return res.status(200).send('client Invoice already exist, try with different')
        }

        var user_id = await merchantApikey.findOne({ where: { key: req.body.apiKey } });
        user_id = user_id.dataValues.merchantId;
        console.log("user_id: ", user_id);
        if (user_id == null) {
            return res.json({ message: 'this user id does not exist' });
        }
        const chain = req.body.chain;
        // const { user_id, symbol, chain } = req.body

        console.log(`Merchant id:${user_id}'s user requests to generate ${req.body.symbol} adddress`)

        const requestOps = {
            command: "create_account",
            commandTopic: chain.toString().toLowerCase() + ".wallet.command",
            data: {
                user_id: user_id,
                symbol: req.body.symbol,
                clientInvoice: req.body.clientInvoice,
                clientName: req.body.clientName,
                chain
            }
        }

        kafka.send(requestOps.commandTopic, requestOps)

        await sleep(2000)

        var address = await Address.findOne({ where: { clientInvoice: req.body.clientInvoice}});
        console.log("address sending is: ", address.dataValues.btcAddress);
        res.status(200).send(address.dataValues.btcAddress)
        // res.status(200).send('Request Received')
    },

    getAddress: async (req, res) => {
        var address = await Address.findOne({ where: { clientInvoice: req.query.clientInvoice}});
        console.log("address sending is: ", address.dataValues.btcAddress);

        res.status(200).send(address.dataValues.btcAddress)
    },


    initAddressWatcher: () => {
        console.log(`Address watcher started`)
        kafka.consumer_on_data((response => {
            if (!concernedTopic(response.topic))
                return

            response = JSON.parse(response.value)

            if (!response.address)
                return

            console.log('Getaddress response: ', response.address)

            const newAddress = {
                btcAddress: response.address,
                merchantId: response.user_id,
                clientInvoice: response.clientInvoice,
                clientName: response.clientName
                // clientName: req.body.clientName,
                // apiKey: req.body.apiKey,
                // clientInvoice: req.body.clientInvoice,
                // isDeposit: false,
                // amount: req.body.amount
            };

            // Save address corresponding to merchant in the database
            Address.create(newAddress)
                .then(data => {
                    console.log("New address created and inserted in db");
                })
                .catch(err => {
                   console.log("error: ", err);
                });

            // const record =
            // {
            //     address: response.address,
            //     user_id: response.user_id,
            //     symbol: response.symbol,
            //     updatedAt: moment.unix()
            // }

            // db.insertRecord(record, 'address').then((recordInserted) => {
            //     log.info(`${record.user_id} ${record.symbol} adddress is ${record.address}`)

            //     let eventObj = Object.assign({}, record)
            //     eventObj.type = 'newAddress'

            //     queue.send(eventhub, eventObj)
            // }).catch((err) => {
            //     log.error(err)
            // })
        }))
    }
}

function concernedTopic(topic) {
    return _.indexOf(filteredTopics, topic) > -1
}

function sleep(timeout) {
    return new Promise(async (resolve) => {
        console.log(`process sleeped for ${timeout}`)
        setTimeout(resolve, timeout)
    })
}