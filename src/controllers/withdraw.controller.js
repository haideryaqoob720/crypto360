'use strict'

const config = require('config')
const bignumber = require('bignumber.js')
const uuidv4 = require('uuid/v4')
var WAValidator = require('wallet-address-validator');

const db = require('../db/db')
const helper = require('../helpers/helper')

const log = require('../helpers/logger').create('r_withdraw')
const fiatServerTopic = config.get('kafka').producers.topics[4]
const url_verifyWithdraw = config.externalServers.kycServer + '/verify/withdraw/fiat'

/**
 * Withdraw funds
 *
 * @param      {Express Request}   req     Express Request object
 * @param      {Express Response}  res     Express Response object
 * @return     {object}                    true/false or array
 * body: withdrawalAddress, amount, currency, description
 */

module.exports = async (req, response) => {

    try {
        const requestData = Object.assign(
            req.body
        )
        const currencyType = requestData.type
            console.log("req data is: ", requestData);
            return res.status('ok');
        requestData.amount = helper.toFixed(requestData.amount, await helper.getDecimals(requestData.symbol));

        switch (currencyType) {
            case 'crypto': {

                cryptoWithdrawal(requestData)
                    .then(tx => {
                        return sendResponse(tx, response)
                    })
                    .catch(err => {
                        errorHandler(err, response)
                    })

                break
            }
            default:
                return helper.errorHandler(res, 403, 'invalidCurrency')
        }

    } catch (error) {
        errorHandler(error, response)
    }
}

function buildEventData(rawTransaction, processedTx) {

    let command, commandTopic,
        txFee = processedTx.txFee, // only added for fiat withdrawal
        referenceId = processedTx.tx_id,
        amount = processedTx.dataValues.serviceAmount || processedTx.withdrawn_amount,
        contractFee = processedTx.contractFee

    if (processedTx.currencyType == 'crypto') {
        command = 'withdraw'
        commandTopic = processedTx.chain.toLowerCase() + ".wallet.command"
    }

    else if (processedTx.currencyType == 'token') {
        command = 'withdraw_token'
        commandTopic = processedTx.chain.toLowerCase() + ".wallet.command"
    }

    else
        return {}

    let eventData = Object.assign({ command, commandTopic })
    eventData.data = Object.assign(rawTransaction, { amount, referenceId, txFee, contractFee })
    return eventData
}

function buildTransactionData(data) {

    const uuid = uuidv4()
    console.log(`${data.user_id} requests for withdrawal of ${data.currency} ${data.amount} id ${uuid}`)

    return {
        uuid: uuid,
        chain: data.chain,
        amount: data.amount, // this is the actual requested amount
        user_id: data.user_id,
        currency: data.currency,
        currencyType: data.type,
        description: data.description,
        withdrawalAddress: data.withdrawalAddress,
    }
}

function cryptoWithdrawal(data) {
    return new Promise(async (resolve, reject) => {
        try {
            const chain = data.chain == 'TOFT' ? 'ETH' : data.chain

            data.description = data.description || `Withdrawal`

            const transactionData = buildTransactionData(data)
            data.uuid = transactionData.uuid

            const _tx = await helper.processWithdrawal_crypto(
                Object.assign({}, transactionData),
                data.token
            )

            // const isInternal = await isInternalTransfer(data)

            // if (isInternal) {
            //     // these 2 lines will record contractFee as exchangeFee
            //     _tx.network_fee = new bignumber(_tx.network_fee).plus(_tx.contractFee).toString()
            //     _tx.contractFee = 0
            // }

            const processedTx = await helper.writeWithdrawalTransaction(_tx)
            processedTx.dataValues.serviceAmount = _tx.serviceAmount;

         
                const eventData = buildEventData(transactionData, processedTx)
                //event for concerned service 
                helper.sendMessage(eventData.commandTopic, eventData)
                return resolve(processedTx)
        } catch (error) {
            reject(error)
        }
    })
}

function errorHandler(error, response) {
    log.error(error)
    const statusCode = error.statusCode
    if (error.message)
        error = { code: error.message }
    else if (!error.error)
        error = { code: 'unableToProcessRequest' }
    else
        error = error.error.code ? error.error : { code: 'unableToProcessRequest' }
    return response.status(statusCode || 500).send(error)
}

function sendResponse(message, response) {
    return response.status(200)
        .send(helper.withdrawalReqResponse(message))
}

function sleep(timeout) {
    return new Promise(async (resolve) => {
        log.info(`process sleeped for ${timeout}`)
        setTimeout(resolve, timeout)
    })
}
