/*=========================
 * 
 *=========================*/
const config = require('config')

const redis = require('./redis')
const db = require('../models/db')
const helper = require('./helper')
const withdrawals = require('../core/withdrawals');
const { default: BigNumber } = require('bignumber.js');

const wallet_symbol = require('config').get('symbol'); // used to identify and store keys in redis
const commandReply = config.kafka.producer.topics.commandReply;
const errorsTopic = config.kafka.producer.topics.errors;
const log = require('../helpers/logger').create("CommandProcessor");
const { hotWalletCreationConfirmed, transactionConfirmed, revenueWithdrawalConfirmed } = config.kafka.producer.topics
const btcCommandReply = `btc.${commandReply}`
const btcTransactionConfirmed = `btc.${transactionConfirmed}`

module.exports = {

    defaultCnf: async (data) => {
        try {

            log.info(`${data.user_id} request to update default configuaration of ${data.symbol}`);

            const defaults = await db.models().configuration.findOne();

            const isDepositDisabled = defaults.conf.deposit;

            defaults.conf = data.conf;
            defaults.name = data.name;
            defaults.symbol = data.symbol;
            defaults.decimals = '8';
            defaults.feeAccount = data.feeAccount;

            await defaults.save();

            log.info(`Default configurations of ${data.symbol} updated: ${JSON.stringify(data.conf)}`);

            if (isDepositDisabled == false && data.conf.deposit == true) {
                require('../core/BTC_RPC').close();
                require('../core/BTC_RPC').init(startFromLastStoredBlock = false);
            }

            if (isDepositDisabled == true && data.conf.deposit == false) {
                require('../core/BTC_RPC').close();
            }

        } catch (err) {
            const message = `[FAILED] to update default configurations of ${data.chain}`,
                error = err ? err.message : err,
                _data = {
                    command: 'defaultCnf',
                    symbol: data.chain
                };
            return sendError(message, error, _data);
        }
    },

    updateCurrencyConfiguration: async (data = {}) => {
        try {
            log.info(`updating Currency Configuration of ${data.chain}`);

            const updatedRecord = await db.update(
                db.models().configuration,
                {
                    conf: data.conf,
                },
                {
                    symbol: data.chain,
                }
            )

            log.info(`${data.symbol} configuarations updated ${updatedRecord}`);
        } catch (err) {
            const message = `[FAILED] to update default configurations of ${data.chain}`,
                error = err ? err.message : err,
                _data = {
                    command: 'defaultCnf',
                    symbol: data.chain
                };
            return sendError(message, error, _data);
        }
    },

    create_account: async (data) => {
        try {

            log.info(`${data.user_id} request to create ${data.symbol} account`);

            const address = await helper.generateAddress[data.chain.toUpperCase()]();

            helper.broadcastMessage(btcCommandReply, {
                address: address,
                symbol: data.symbol,
                user_id: data.user_id,
            });

        } catch (err) {
            const message = `[FAILED] to create_account against: ${data.user_id} `,
                error = err ? err.message : err,
                _data = {
                    command: 'create_account',
                    user_id: data.user_id,
                };
            return sendError(message, error, _data);
        }
    },

    add_wallet: async (data = {}) => {
        log.info(`New Cold Wallet request ${JSON.stringify(data)} `);
        const address = data.address
        Promise.all([
            db.upsert(
                db.models().wallet,
                {
                    "type": 'cold',
                    "address": address,
                    "symbol": data.chain.toUpperCase(),
                    "target_balance": await helper.convertToBase(data.chain, data.target_balance),
                }),
            redis.get('getAsync', `${data.chain.toLowerCase()}:address:cold:${address}`)
        ]).then(async ([record, value]) => {
            let _value = value
            if (!_value) {
                _value = {
                    symbol: data.chain,
                    addresses: new Array(address)
                }
                _value = JSON.stringify(_value)
                redis.set('setAsync', `${data.chain.toLowerCase()}:address:cold:${address}`, _value)
            }
            log.info(`Cold wallet added: ${address} ${data.chain} ${record} `);
        }).catch((err) => {
            const message = `[FAILED] to add Cold wallet: ${address} `,
                error = err ? err.message : err,
                _data = {
                    command: 'add_wallet',
                    address: address,
                };
            return sendError(message, error, _data);
        })
    },

    withdraw: async (data = {}) => {
        try {
            const chain = data.chain.toUpperCase()
            data.chain = chain
            data.value = helper.convertToBase(chain, data.amount)
            const withdrawTx = await helper.withdraw[data.chain](data);

            const response = {
                from: 'BTC',
                status: true,
                uuid: data.uuid,
                type: 'withdraw',
                chain: data.chain,
                value: data.amount,
                symbol: data.chain,
                user_id: data.user_id,
                tx_id: withdrawTx.hash,
                value_base: data.value,
                to: data.withdrawalAddress,
                timestamp: parseInt(Date.now() / 1000),
                txFee: helper.convertFromBase(chain, withdrawTx.fee)
            }

            helper.broadcastMessage(btcTransactionConfirmed, response);

        } catch (err) {
            log.error(err);
            const message = `[FAILED] withdrawal of ${data.user_id} uuid ${data.uuid} `,
                error = err.message || err || message,
                _data = {
                    command: 'withdraw',
                    uuid: data.uuid,
                };
            return sendError(message, error, _data);
        }
    },

    exchangeHotWallet: async () => {
        const wallets = await db.findAll(
            db.models().wallet,
            { type: 'hot' },
            {
                attributes: ['address', 'symbol', 'balance']
            }
        )
        wallets.map(async wallet => {
            helper.broadcastMessage(hotWalletCreationConfirmed, {
                type: 'hot_wallet_creation_confirmed',
                address: wallet.address,
                symbol: wallet.symbol,
                balance: await helper.convertFromBase(wallet.symbol, wallet.balance),
            });
        })
    },

    revenue_withdraw_crypto: async (data = {}) => {
        try {
            try {
                const chain = data.chain.toUpperCase()
                data.chain = chain
                data.value = helper.convertToBase(chain, data.amount)
                const withdrawTx = await helper.withdraw[data.chain](data);

                const response = {
                    from: 'BTC',
                    status: true,
                    uuid: data.uuid,
                    type: 'withdraw',
                    chain: data.chain,
                    value: data.amount,
                    symbol: data.chain,
                    user_id: data.user_id,
                    tx_id: withdrawTx.hash,
                    value_base: data.value,
                    to: data.withdrawalAddress,
                    timestamp: parseInt(Date.now() / 1000),
                    txFee: helper.convertFromBase(chain, withdrawTx.fee)
                }

                helper.broadcastMessage(revenueWithdrawalConfirmed, response);

            } catch (err) {
                log.error(err);
                const message = `[FAILED] withdrawal of ${data.user_id} uuid ${data.uuid} `,
                    error = err.message || err || message,
                    _data = {
                        command: 'revenue_withdraw_crypto',
                        uuid: data.uuid,
                    };
                return sendError(message, error, _data);
            }
        } catch (error) {

        }
    },

    maxWithdraw: async (data = {}) => {
        try {

            const _symbol = data.symbol.toUpperCase();
            const { amount, fee } = await withdrawals.estimatedFee[_symbol]();
            const maxWithdraw = new BigNumber(amount).minus(fee);

            return helper.broadcastMessage(btcCommandReply, {
                maxAmount: await helper.convertFromBase(_symbol, maxWithdraw),
                status: 'resolve',
                requestId: data.requestId,
            })

        } catch (error) {

            const response = {
                status: 'reject',
                error: error.message,
                requestId: data.requestId,
            }

            return helper.broadcastMessage(btcCommandReply, response)
        }
    },

    network_fee: async (data = {}) => {
        try {

            const _symbol = data.symbol.toUpperCase();
            const { fee } = await withdrawals.estimatedFee[_symbol](data.amount);

            return helper.broadcastMessage(btcCommandReply, {
                fee: await helper.convertFromBase(_symbol, fee),
                status: 'resolve',
                requestId: data.requestId,
            })

        } catch (error) {

            const response = {
                status: 'reject',
                error: error.message,
                requestId: data.requestId,
            }

            return helper.broadcastMessage(btcCommandReply, response)
        }
    }
}

function sendError(message, error, data) {
    log.error('[message]: ', message, ' [error]:', error)
    const errorMessage = {
        service: wallet_symbol,
        message: message,
        error: error,
        data: data
    }
    helper.broadcastMessage(errorsTopic, errorMessage)
}

setTimeout(() => {
    console.log('Command processor 384: REMOVE THIS FUNCTION,ONLY TO SUPPORT V1.0')
    module.exports.exchangeHotWallet()
}, 4000)