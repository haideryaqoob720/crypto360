kafka = require('../helpers/kafka');
const btcCommandReply = 'btc.wallet.command';

const db = require("../models");
const Address = db.address;
const User = db.user;
const Deposits = db.deposits;

const Op = db.Sequelize.Op;
const _ = require('underscore')
const config = require('../../config/_local')
const consumerTopics = config.kafka.consumer.topics //getting objects of supported chains
const event_deposit = config.kafka.event_hub.producers.topics[1]
const event_withdraw = config.kafka.event_hub.producers.topics[2]
const revenue_withdrawal_confirmed = config.kafka.consumer.topics[19];
/**
 * Listen for incomming events from wallet services 
 */
module.exports = {

    initWatcher: () => {
        console.log('deposit withdraw watcher started')
        kafka.consumer_on_data(async function txListner(response) {
            // try {

            if (!concernedTopic(response.topic))
                return

            const eventData = JSON.parse(response.value)

            console.log('Transaction received in init watcher: ', eventData)

            if (eventData.type == 'deposit') {
                console.log("it's deposit event ", eventData.to)

                var deposit = await Address.findOne({ where: { btcAddress: eventData.to } });
                console.log('deposit amount is :', eventData.value);
                const depositAmount = eventData.value;
                const depositTransactionId = eventData.tx_id;
                const depositTime = eventData.timestamp;
                const depositAddress = eventData.to;
                const userId = deposit.dataValues.merchantId;
                const clientInvoiceNumber = deposit.dataValues.clientInvoice;
                const clientName = deposit.dataValues.clientName;

                await Deposits.create({
                    merchantId: userId,
                    depositAmount,
                    depositTransactionId,
                    depositTime,
                    depositAddress,
                    clientInvoiceNumber,
                    clientName
                });

                if (deposit == null) {
                    return res.json({ message: 'this address does not exist' });
                }

                await Address.update({
                    isDeposit: true,
                    amount: eventData.value
                },
                    {
                        where: {
                            btcAddress: eventData.to
                        }
                    });

                

                // const userId = deposit.dataValues.merchantId;
                var balance = await User.findOne({ where: { id: userId } });
                console.log(`balance of userId: ${userId} is ${balance.dataValues.balance}`)
                var oldBalance = balance.dataValues.balance;
                var currentBalance = oldBalance + depositAmount;
                console.log(`userId: ${userId} and balance is ${currentBalance}`);
                await User.update({
                    balance: currentBalance
                },
                    {
                        where: {
                            id: userId
                        }
                    });
                console.log('balance updated');

            }
            else if (eventData.type == 'withdraw') {
                const withdrawAmount = eventData.value;
                var userId = eventData.user_id;
                var balance = await User.findOne({ where: { id: userId } });
                console.log(`balance of userId: ${userId} is ${balance.dataValues.balance}`)
                var oldBalance = balance.dataValues.balance;
                var currentBalance = oldBalance - withdrawAmount;
                await User.update({
                    balance: currentBalance
                },
                    {
                        where: {
                            id: userId
                        }
                    });
                console.log('balance updated');
            }

            // else if (eventData.type == 'withdraw') {

            //     eventData.status = eventData.status == false ? false : true;

            //     const { OMSEvent, traderEvent, tx } = await helper.confirmWithdrawal(eventData);
            //     OMSEvent.transaction = await helper.orderAPIData_withdraw({ id: tx.id });

            //     if (eventData.email)
            //         OMSEvent.transaction.receiver = eventData.email

            //     helper.notifyOrderApi(event_withdraw, OMSEvent)
            //     helper.broadcastEvent(traderEvent)

            //     // deduct txFee from hotwallet
            //     if (tx.txFee > 0)
            //         await holdings.onWithdrawal({
            //             value: tx.txFee,
            //             chain: tx.chain,
            //             symbol: tx.chain
            //         })


            //     if (eventData.isInternal) {
            //         await accounting.user2user(tx)
            //     }
            //     else {
            //         let amount = new bigNumber(tx.withdrawn_amount).gte(tx.amount) ?
            //             new bigNumber(tx.amount).plus(tx.contractFee) : new bigNumber(tx.withdrawn_amount);
            //         //updating global holdings
            //         await holdings.onWithdrawal({
            //             value: amount,
            //             chain: tx.chain,
            //             symbol: tx.symbol
            //         })

            //         if (eventData.status)
            //             accounting.withdraw(tx)
            //         else
            //             accounting.withdrawFailed(tx)
            //     }

            //     // verify if hotwallet balance is underrun
            //     sendLowBalanceAlerts()
            // }

            // else if (eventData.type == 'hot_wallet_creation_confirmed') {

            //     const data = {
            //         type: 'hot',
            //         symbol: eventData.symbol,
            //         address: eventData.address,
            //         target_balance: 0,
            //         current_balance: parseFloat(eventData.balance || 0).toFixed(8),
            //         createdAt: Math.floor(Date.now() / 1000),
            //     }

            //     const addressInserted = await db.upsertRecord(data, 'wallet')
            //     log.info(`hot wallet ${data.symbol} ${data.address} added`)
            // }

            // } catch (error) {
            //     log.error(error)
            // }
        })
        // updateOptimalThreshold()
        // setInterval(updateOptimalThreshold, optimalThresholdUpdateInterval)
    }
}

function isDepositEnabled(currency) {
    return new Promise((resolve, reject) => {
        db.getRecord(['deposit'], { symbol: currency }, 'currency').then(result => {
            resolve(result ? result.deposit : false);
        }).catch(reject);
    })
}

//filtering only topics haiving confirmed 
var filteredTopics = _.filter(consumerTopics, function (topic) {
    return topic.toString().indexOf("confirmed") > -1
})

function concernedTopic(topic) {
    return (_.indexOf(filteredTopics, topic) > -1) && topic != revenue_withdrawal_confirmed
}