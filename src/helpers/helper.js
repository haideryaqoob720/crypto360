queue = require("../helpers/kafka");
const db = require("../models");
const Transaction = db.transaction;
module.exports = {

    init: () => {
        return new Promise(async (resolve, reject) => {
            try {
                await queue.connectProducer()
                await queue.connectConsumer()

                //watchers
                require('../helpers/crypto_watcher').initWatcher();
                require('../controllers/newAddress.controller').initAddressWatcher();

                resolve()
            } catch (error) {
                console.log(error)
                await module.exports.closeConnections()
                return reject(error);
            }
        })
    },

    broadcastMessage: (topic, message) => {
        return queue.send(topic, message);
    },

    sendMessage: (topic, message) => {
        queue.send(topic, message)
    },

    updateDeposit: (tx) => {
        return new Promise(async (resolve, reject) => {
            try {
                console.log(`Updating deposit against address ${tx.to}`)

                let record = await db.getRecord(
                    ['user_id', 'symbol'],
                    {
                        'address': tx.to
                    },
                    'address'
                )

                if (!record) {
                    throw new Error(`[DEPOSIT] address ${tx.to} not found`)
                }

                if (tx.symbol != record.symbol)
                    log.warn(`[DEPOSIT] address ${tx.to} found ${record.symbol} expected ${tx.symbol}`)

                const user_id = record.user_id

                // http post request body
                const reqOp = {
                    'user_id': user_id,
                    'symbol': tx.symbol,
                    amount: tx.value.toString()
                }

                const txObjForDb = {
                    uuid: tx.tx_id.toString().substring(0, 36),
                    user_id,
                    type: 'deposit',
                    amount: tx.value,
                    sender: tx.from,
                    receiver: tx.to,
                    tx_id: tx.tx_id,
                    symbol: tx.symbol,
                    status: 'success',
                    createdAt: tx.timestamp,
                    chain: tx.chain,
                    description: tx.description ? (tx.description === 'Withdrawal' ? 'Deposit' : tx.description)
                        : `No Message`
                }

                const recordInserted = await db.insertRecord(txObjForDb, 'transaction')

                // deposit event amount should be in base amount
                const deposit = {
                    amount: await module.exports.convertToBase(txObjForDb.amount, txObjForDb.symbol),
                    symbol: txObjForDb.symbol,
                    user_id: user_id,
                }

                const event = {
                    id: recordInserted.dataValues.id,
                    user_id: user_id,
                    amount: txObjForDb.amount,
                    chain: txObjForDb.chain,
                    status: txObjForDb.status,
                    symbol: txObjForDb.symbol,
                    createdAt: tx.timestamp,
                    tx_id: txObjForDb.tx_id,
                    type: 'deposit',
                    description: txObjForDb.description,
                    sender: tx.from,
                    receiver: exchangeName
                }

                log.info(`${user_id} deposit of ${event.symbol} ${event.amount} recorded`)

                return resolve({ txDetails: recordInserted })

            } catch (error) {
                log.error(`unable to write deposit against of ${tx.symbol} ${error.message || error}`)
                reject(error)
            }
        })
    },

    writeWithdrawalTransaction: (tx) => {
        return new Promise((resolve, reject) => {
            console.log(`[WITHDRAWAL] writing transaction ${tx.uuid} status success `)
            //tx obj to write into db
            const txObjForDb = {
                uuid: tx.uuid,
                type: 'withdraw',
                chain: tx.chain,
                txFee: 0,
                status: 'success',
                amount: tx.amount,
                symbol: tx.currency,
                user_id: tx.user_id,
                sender: tx.sender || null,
                description: tx.description,
                network_fee: 0,
                currencyType: 'crypto',
                receiver: tx.withdrawalAddress,
                tx_id: tx.reference_id || '0x0',
                contractFee: tx.contractFee || 0,
                withdrawn_amount: tx.amount
            }

            Transaction.create(txObjForDb).then(resolve).catch(reject)
        })
    },

    withdrawalReqResponse: (tx) => {
        return {
            id: tx.id,
            user_id: tx.user_id,
            tx_id: tx.tx_id,
            receiver: tx.receiver,
            sender: tx.exchangeName,
            amount: tx.withdrawn_amount,
            chain: tx.chain,
            status: tx.status,
            symbol: tx.symbol,
            createdAt: tx.createdAt,
            type: tx.type,
            description: tx.description
        }
    },
    errorHandler: (res, statusCode, errorCode, trace_id) => {
        return res.status(statusCode).send({ code: errorCode, trace_id })
    },
    closeConnections: () => {
        return new Promise(async (resolve, reject) => {
            queue.closeConnections()
            // db.closeConnection()
            // await logger.shutdown()
            setTimeout(resolve, 1000); // resolve after 1 sec
        })
    },
}