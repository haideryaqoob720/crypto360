module.exports = (sequelize, Sequelize) => {
    const DepositTx = sequelize.define('DepositTxs', {
        merchantId: {type: Sequelize.STRING, allowNull: false},
        depositAmount: { type: Sequelize.STRING, allowNull: false, defaultValue: 0, validate: { min: 0 } },
        depositAddress: { type: Sequelize.STRING(255), allowNull: false },
        depositTime: { type: Sequelize.INTEGER(15), allowNull: false, defaultValue: 0 },
        depositTransactionId: { type: Sequelize.STRING(66), allowNull: true },
        status: { type: Sequelize.ENUM('success', 'pending'), allowNull: false, defaultValue: 'success' },
        clientInvoiceNumber:  { type: Sequelize.INTEGER(15), allowNull: false, defaultValue: 0 },
        clientName: { type: Sequelize.STRING(66), allowNull: true }
    }, {
        timestamps: false
    })
  
    return DepositTx;
  };