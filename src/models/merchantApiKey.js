module.exports = (sequelize, Sequelize) => {
    const MerchantApiKey = sequelize.define("merchantApiKeys", {
      shopName: {
        type: Sequelize.STRING
      },
      key: {
        type: Sequelize.STRING
      }
    });
  
    return MerchantApiKey;
  };