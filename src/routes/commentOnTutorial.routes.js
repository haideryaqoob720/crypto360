module.exports = app => {
    const tutorials = require("../controllers/controller.js");
  
    var router = require("express").Router();
  
    router.post("/createNewTutorial", tutorials.createTutorial);
  
    router.post("/create", tutorials.createComment);
  
    router.get("/:id", tutorials.findTutorialById);
  
    router.get("/:id", tutorials.findCommentById);
  
    router.get("/", tutorials.findAll);
  
    app.use('/api/comment', router);
  };