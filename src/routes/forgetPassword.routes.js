var crypto = require("crypto");
var bcrypt = require("bcryptjs");
var nodemailer = require('nodemailer');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const db = require("../models");
const ResetToken = db.resetToken;
const User = db.user;

module.exports = app => {
    // const forgetPassword = require("../controllers/forgetPassword.controller");

    var router = require("express").Router();

    router.post("/", async function (req, res, next) {
        //ensure that you have a user with this email
        var email = await User.findOne({ where: { email: req.body.email } });
        if (email == null) {
            /**
             * we don't want to tell attackers that an
             * email doesn't exist, because that will let
             * them use this form to find ones that do
             * exist.
             **/
            return res.json({ status: 'ok' });
        }
        /**
         * Expire any tokens that were previously
         * set for this user. That prevents old tokens
         * from being used.
         **/
        await ResetToken.update({
            used: 1
        },
            {
                where: {
                    email: req.body.email
                }
            });

        //Create a random reset token
        var token = crypto.randomBytes(64).toString('base64');

        //token expires after one hour
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 1);

        //insert token data into DB
        await ResetToken.create({
            email: req.body.email,
            expiration: expireDate,
            token: token,
            used: 0
        });
        console.log("token: ", token);
        console.log("email: ", req.body.email);
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'haideryaqoobengr@gmail.com',
                pass: '012854136'
            }
        });

        var mailOptions = {
            from: 'haideryaqoobengr@gmail.com',
            to: req.body.email,
            subject: 'FORGET PASSWORD',
            text: 'To reset your password, please click the link below.\n\nhttp://' + 'localhost:8080' + '/Resetpass?token=' + encodeURIComponent(token) + '&email=' + req.body.email
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });

        return res.json({ status: 'ok' });
    });

    router.post("/changePassword", async function (req, res, next) {
        /**
         * This code clears all expired tokens. You
         * should move this to a cronjob if you have a
         * big site. We just include this in here as a
         * demonstration.
         **/
        await ResetToken.destroy({
            where: {
                expiration: { [Op.lt]: Sequelize.fn('CURDATE') },
            }
        });
        console.log("email is: ", req.body.email);
        console.log("token is: ", req.body.token);
        //find the token
        var record = await ResetToken.findOne({
            where: {
                email: req.body.email,
                expiration: { [Op.gt]: Sequelize.fn('CURDATE') },
                token: req.body.token,
                used: 0
            }
        });

        if (record == null) {
            return res.json({ status: 'error', message: 'Token not found. Please try the reset password process again.' });
        }

        //compare passwords
        if (req.body.password1 !== req.body.password2) {
            return res.json({ status: 'error', message: 'Passwords do not match. Please try again.' });
        }

        /**
        * Ensure password is valid (isValidPassword
        * function checks if password is >= 8 chars, alphanumeric,
        * has special chars, etc)
        **/
        // if (!isValidPassword(req.body.password1)) {
        //     return res.json({ status: 'error', message: 'Password does not meet minimum requirements. Please try again.' });
        // }

        var upd = await ResetToken.update({
            used: 1
        },
            {
                where: {
                    email: req.body.email
                }
            });

        var newSalt = crypto.randomBytes(64).toString('hex');
        console.log("newSalt: ", newSalt);
        // var newPassword = crypto.pbkdf2Sync(req.body.password1, newSalt, 10000, 64, 'sha512').toString('base64');

        await User.update({
            password: bcrypt.hashSync(req.body.password1, 8),
            // salt: newSalt
        },
            {
                where: {
                    email: req.body.email
                }
            });

        return res.json({ status: 'ok', message: 'Password reset. Please login with your new password.' }); res.render('user/reset-password', {
            showForm: true,
            record: record
        });
    });

    app.use('/merchant/forgetPassword', router);

};