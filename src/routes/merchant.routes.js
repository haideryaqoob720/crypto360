const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const db = require("../models");
const User = db.user;
const Address = db.address;
const Transaction = db.transaction;
const Deposits = db.deposits;

module.exports = app => {
    // const forgetPassword = require("../controllers/forgetPassword.controller");

    var router = require("express").Router();

    router.get("/balance", async function (req, res, next) {

        var balance = await User.findOne({ where: { id: req.query.merchantId } });
        if (balance == null) {
            return res.json({ message: 'user not exist' });
        }
        console.log(`Balance of merchant: ${req.query.merchantId} is ${balance.dataValues.balance}`);
        return res.json({ amount: balance.dataValues.balance });
    });

    router.get("/withdrawTx", async function (req, res, next) {

        const merchantId = req.query.merchantId;
        // var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;
        var condition = {user_id: merchantId}
      
        Transaction.findAll({ where: condition })
          .then(data => {
            res.send(data);
          })
          .catch(err => {
            res.status(500).send({
              message:
                err.message || "Some error occurred while retrieving transactions."
            });
          });
  
    });

    router.get("/depositTx", async function (req, res, next) {

      const merchantId = req.query.merchantId;
      // var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;
      var condition = {merchantId: merchantId}
    
      Deposits.findAll({ where: condition })
        .then(data => {
          res.send(data);
        })
        .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving transactions."
          });
        });

  });

    app.use('/merchant', router);

};