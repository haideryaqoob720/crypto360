module.exports = app => {
    const shopKeys = require("../controllers/merchantApiKey");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", shopKeys.create);
  
    // Retrieve all Tutorials
    // router.get("/all:id", shopKeys.findAllApiKeysOfMerchant);
  
    // Retrieve all published Tutorials
    router.get("/", shopKeys.findAll);
  
    // // Retrieve a single Tutorial with id
    // router.get("/:id", tutorials.findOne);
  
    // // Update a api key with id
    router.put("/:id", shopKeys.update);
  
    // // Delete a Tutorial with id
    router.delete("/:id", shopKeys.delete);
  
    // // Delete all Tutorials
    // router.delete("/", tutorials.deleteAll);
  
    app.use('/merchant/apiKey', router);
  };